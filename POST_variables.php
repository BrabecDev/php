<?php 

	$nameArray = array('lucas', 'nilma', 'raphael', 'irineu', 'borracha');

	if ($_POST) {

		#echo $_POST['number']."<br/>";
		#print_r($_POST);

		$i = 0;
		$found = false;

		while ($i < sizeof($nameArray)) {
			
			if ($_POST['name'] == $nameArray[$i]) {

				$found = true;
				break;
			}

			$i++;
		}

		if ($found) {
			echo "<p>Hello, ".$_POST['name']."</p>";
		}else {
			echo "<p>You're just somebody that I used to know!</p>";
		}
		
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>POST Variables</title>
</head>
<body>
	<form method="POST">
		<label>What's your name?</label>
		<input type="text" name="name">
		<input type="submit" name="" value="send">
	</form>
</body>
</html>