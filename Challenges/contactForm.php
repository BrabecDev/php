<?php 
	
	if ($_POST) {
		
		$emailTo = "lu_brabec@hotmail.com";
		$subject = $_POST['subject'];
		$message = $_POST['body'];
		$sender = $_POST['sender'];
		$headers = "From: ".$sender;

		$sent = false;

		if (mail($emailTo, $subject, $message, $headers)) {
			$sent = true;
		}

	}

?>

<html>
	<head>
		<meta charset="utf-8">
		<title>CHALLENGE - A Contact Form</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	</head>
	<body>
		<div class="container">
			<div class="col-md-10">
				
				<h1>Get in touch!</h1>
				<div id="error"></div>

				<form method="post">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Email address</label>
				    <input required name="sender" type="email" class="form-control" id="sender" aria-describedby="emailHelp" placeholder="Enter email">
				    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
				  </div>
				  <div class="form-group">
				    <label>Subject</label>
				    <input id="subject" type="text" class="form-control" name="subject">
				  </div>
				  <div class="form-group">
				    <label>What would you like to ask us?</label>
				    <textarea id="body" name="body" class="form-control" maxlength="500"></textarea>
				  </div>
				  <button id="submit" type="submit" class="btn btn-primary">Submit</button>
				</form>
			</div>
		</div>
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>

		<script type="text/javascript" charset="utf-8" async defer>

			$("form").submit(function (e) {
				e.preventDefault();

				var error = "";

				if ($("#subject").val() == "") {

					error += "The subject field is required.<br/>";
				}

				if ($("#body").val() == "") {

					error += "The body field is required.<br/>";
				}

				if (error != "") {

					$("#error").html('<div class="alert alert-danger"><p><strong>There were error(s) in your form:</strong></p> '+ error +'</div>');
				}else{

					$("#error").html('<div class="alert alert-success"><strong>Success!</strong> Your email was sent successfully!</div>');
				}

			});

		</script>
	</body>
</html>