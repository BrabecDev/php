<?php 
for ($i=0; $i < 10; $i++) { 
	echo $i."<br/>";
}

#even-numbers
echo "<br/><br/>";
for ($i=2; $i <= 30; $i++) { 
	if ($i % 2 == 0) {
		echo $i."<br/>";
	}
}

#countdown
echo "<br/><br/>";
for ($i=10; $i >= 0 ; $i--) { 
	echo $i."<br/>";
}

#family-array
echo "<br/><br/>";
$family = array("Brabec", "Santana", "Ferro", "Pinheiro", "Fontes", "Trevor");

for ($i=0; $i < sizeof($family); $i++) { 
	echo $family[$i]."<br/>";
}

echo "<br/><br/>";
foreach ($family as $key => $value) {
	$family[$key] = $value." Something";

	echo "Array item: ".$key." is ".$value."<br/>";
}

echo "<br/><br/>";
foreach ($family as $key => $value) {
	echo "Array item: ".$key." is ".$value."<br/>";
}
?>